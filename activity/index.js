//1.

const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);


//2
let address = `I live at 258 Washington Ave NW, California 90011`;

console.log(`${address}`);



const animal = ["Lolong", "1075 kgs", "20 ft 3in" ];
const [animalName, weight, measurement] = animal;


console.log(`${animalName} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${measurement}. `);


const numbers = [5, 4, 3, 2, 1];
numbers.forEach((number) => {
	console.log(`${number}`);
});

const reduceNumber = numbers.reduce((number1, next) => number1 + next, 0);
console.log(reduceNumber);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
}

const myDog = new Dog();
myDog.name = "Brownie";
myDog.age = "10";
myDog.breed = "Labrador";

console.log(myDog);

